## Ecommerce UI Test

### Tecnologias
* Javascript
* Node/Npm
* Cypress

### Execução dos testes sem Cypress GUI

`npm install`

`npm run test:functional`

### Execução dos testes com Cypress GUI

`npm install`

`npm run test:functional:open`

`Selecionar o spec ShoppingCartFlow`

### Screenshots e Videos

Acessar screenshots e vídeos após execução dos testes em:

`test/screenshots`

`test/videos`

### Suíte e Casos de testes

O documento está no pdf "Suítes de Teste - Ecommerce.pdf", na raiz do projeto.