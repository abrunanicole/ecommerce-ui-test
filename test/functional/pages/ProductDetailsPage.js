class ProductDetailsPage {
  constructor() {
    this.cy = cy;
  }

  productDetailTitle() {
    return this.cy.get('.header-product__title');
  }

  addToCartButton() {
    return this.cy.get('.js-add-cart-button.js-main-add-cart-button.js-add-box-prime');
  }
}

export default ProductDetailsPage;