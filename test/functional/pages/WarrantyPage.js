class WarrantyPage {
  constructor() {
    this.cy = cy;
  }

  continueButton() {
    return this.cy.get('.btn-buy-warranty');
  }
}

export default WarrantyPage;