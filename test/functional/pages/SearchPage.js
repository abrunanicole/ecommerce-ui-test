class SearchPage {
  constructor() {
    this.cy = cy;
  }

  searchInput() {
    return this.cy.get('#inpHeaderSearch');
  }

  searchButton() {
    return this.cy.get('#btnHeaderSearch');
  }
}

export default SearchPage;