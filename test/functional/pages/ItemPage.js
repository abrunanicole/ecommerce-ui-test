class ItemPage {
  constructor(item) {
    this.item = item;
  }

  title () {
    return this.item.find('.BasketItemProduct-info-title > p');
  }

  quantity() {
    return this.item.find('.BasketItemProduct-quantity-dropdown');
  }

  remove() {
    return this.item.find('.BasketItemProduct-quantity-remove');
  }

  price() {
    return this.item.find('.BasketItemProduct-price');
  }
}

export default ItemPage;