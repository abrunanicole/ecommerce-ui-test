import ItemPage from './ItemPage';

class ShoppingCartPage {
  constructor() {
    this.cy = cy;
  }

  pageTitle() {
    return this.cy.get('.BasketPage-title');
  }

  itemsList() {
    return this.cy.get('.BasketItem');
  }

  item (index) {
    return new ItemPage(this.itemsList().eq(index));
  }
}

export default ShoppingCartPage;