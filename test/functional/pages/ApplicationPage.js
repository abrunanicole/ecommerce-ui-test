import SearchPage from './SearchPage';
import SearchResultPage from './SearchResultPage';
import ProductPage from './ProductPage';
import ProductDetailsPage from './ProductDetailsPage';
import WarrantyPage from './WarrantyPage';
import ShoppingCartPage from './ShoppingCartPage';
import ItemPage from './ItemPage';

class ApplicationPage {
  constructor() {
    this.searchPage = new SearchPage();
    this.searchResultPage = new SearchResultPage();
    this.productPage = new ProductPage();
    this.productDetailsPage = new ProductDetailsPage();
    this.warrantyPage = new WarrantyPage();
    this.shoppingCartPage = new ShoppingCartPage();
    this.itemPage = new ItemPage();
    this.cy = cy;
  }

  goToHomePage() {
    this.cy.visit('https://www.magazineluiza.com.br/');
    return this;
  }

  search() {
    return this.searchPage;
  }

  searchResult() {
    return this.searchResultPage;
  }

  product() {
    return this.productPage;
  }

  productDetails() {
    return this.productDetailsPage;
  }

  warranty() {
    return this.warrantyPage;
  }

  shoppingCart() {
    return this.shoppingCartPage;
  }

  item() {
    return this.itemPage;
  }
}

export default ApplicationPage;
