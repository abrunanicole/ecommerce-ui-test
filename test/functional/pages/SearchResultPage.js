import ProductPage from './ProductPage';

class SearchResultPage {
  constructor() {
    this.cy = cy;
  }

  resultTitle() {
    return this.cy.get('.header-search > h1');
  }

  productList() {
    return this.cy.get('.product');
  }

  product(index) {
    return new ProductPage(this.productList().eq(index));
  }
}

export default SearchResultPage;