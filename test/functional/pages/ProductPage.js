class ProductPage {
  constructor(product) {
    this.product = product;
  }

  productImage() {
    return this.product.find('img');
  }

  productName() {
    return this.product.find('.productTitle');
  }

  productOldPrice() {
    return this.product.find('.originalPrice');
  }

  productNewPrice() {
    return this.product.find('.price');
  }

  productInstallment() {
    return this.product.find('.installmentPrice');
  }

  productLink() {
    return this.product.find('img');
  }
}

export default ProductPage;