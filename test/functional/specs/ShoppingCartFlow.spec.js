import ApplicationPage from '../pages/ApplicationPage';

const application = new ApplicationPage();

describe('Shopping Cart Flow Validation', () => {
  it('visits the home page', () => {
    application.goToHomePage();
  });

  it('searches for a product', () => {
    application.search()
      .searchInput()
      .type('Bicicleta Aro 26 Mountain Bike Track Bikes - TB 200/PP 18 Marchas');

    application.search()
      .searchButton()
      .click();

    application.searchResult()
      .resultTitle()
      .should('contain.text', 'Bicicleta Aro 26 Mountain Bike Track Bikes - TB 200 PP 18 Marchas');
  });

  it('validates first product', () => {
    application.searchResult()
      .product(0)
      .productImage()
      .should('be.visible');

    application.searchResult()
      .product(0)
      .productName()
      .should('contain.text', 'Bicicleta Aro 26 Mountain Bike Caloi Andes  - Freio V-Brake 21 Marchas');

    application.searchResult()
      .product(0)
      .productOldPrice()
      .should('contain.text', 'de R$ 1.099,00 por');

    application.searchResult()
      .product(0)
      .productNewPrice()
      .should('contain.text', 'R$ 712,41')
      .should('contain.text', 'à vista');

    application.searchResult()
      .product(0)
      .productInstallment()
      .should('contain.text', 'em até 12x de R$ 62,49 sem juros');
  });

  it('selects the first product', () => {
    application.searchResult()
      .product(0)
      .productLink()
      .click();

    application.productDetails()
      .productDetailTitle()
      .should('contain.text', 'Bicicleta Aro 26 Mountain Bike Caloi Andes  - Freio V-Brake 21 Marchas');
  });

  it('adds product to shopping cart', () => {
    application.productDetails()
      .addToCartButton()
      .click();

    application.warranty()
      .continueButton()
      .click();
  });

  it('validates shopping cart information', () => {
    application.shoppingCart()
      .item(0)
      .title()
      .should('contain.text', 'Bicicleta Aro 26 Mountain Bike Caloi Andes - Freio V-Brake 21 Marchas');

    application.shoppingCart()
      .item(0)
      .quantity()
      .should('have.value', '1');

    application.shoppingCart()
      .item(0)
      .remove()
      .should('be.visible');

    application.shoppingCart()
      .item(0)
      .price()
      .should('contain.text', 'R$ 1.099,00')
      .should('contain.text', 'R$ 749,90')
      .should('contain.text', 'R$ 712,41');
  });
});
